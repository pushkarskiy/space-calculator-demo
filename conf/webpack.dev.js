import { resolve, join } from 'path'
import webpack from 'webpack'
import { devServer } from './config.json'

process.env.NODE_ENV = 'development'
process.traceDeprecation = true

export default {
  context: resolve('src'),

  entry: {
    'widget-common': ['react-hot-loader/patch', 'babel-polyfill', './utils/initialize.js'],
    'widget-calc': ['./entries/calc'],
  },

  output: {
    filename: '[name].js',
    pathinfo: true,
    path: join(__dirname, 'build'),
    publicPath: '/build/',
  },

  resolve: {
    modules: ['./node_modules'],
    extensions: ['.js', '.jsx', '.json', '.css', '.styl'],
    alias: {
      '~': resolve('src'),
      conf: resolve('conf'),
    },
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: resolve('.'),
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
      },
      {
        test: /\.styl$/,
        use: [
          {
            loader: require.resolve('style-loader'),
          },
          {
            loader: require.resolve('css-loader'),
            options: {
              importLoaders: 1,
              minimize: false,
              sourceMap: true,
              modules: true,
              localIdentName: '[local]--[hash:base64:4]',
            },
          },
          {
            loader: require.resolve('postcss-loader'),
            options: {
              sourceMap: true,
              plugins: () => [require('postcss-cssnext'), require('postcss-assets'), require('postcss-flexbugs-fixes')],
            },
          },
          {
            loader: require.resolve('stylus-loader'),
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: require.resolve('style-loader'),
          },
          {
            loader: require.resolve('css-loader'),
            options: {
              modules: false,
            },
          },
        ],
      },
    ],
  },

  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    'react-dom/server': 'ReactDOMServer',
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),

    new webpack.NoEmitOnErrorsPlugin(),
  ],

  stats: {
    assets: true,
    cached: true,
    errors: true,
    errorDetails: true,
    hash: true,
    timings: true,
    version: true,
    warnings: true,
  },

  devtool: 'inline-source-map',

  watch: false,

  devServer: {
    inline: false,
    hot: true,
    port: devServer.port,
    lazy: false,
    historyApiFallback: true,
    contentBase: [join(__dirname, '../assets'), join(__dirname, '../build')],
    overlay: true,
    watchContentBase: true
  },
}
