import webpack from 'webpack'
import { resolve, join } from 'path'
import ManifestPlugin from 'webpack-manifest-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import autoprefixer from 'autoprefixer'

process.env.NODE_ENV = 'production'
process.env.BABEL_ENV = 'production'

const outputPath = join(__dirname, '../../dist/')
const fileNamePatern = '[name].[chunkhash]'

export default {
  context: resolve('src'),

  entry: {
    'widget-common': ['react-hot-loader/patch', 'babel-polyfill', './utils/initialize.js'],
    'widget-calc': ['./entries/calc'],
  },

  output: {
    filename: fileNamePatern + '.js',
    pathinfo: true,
    path: outputPath,
    publicPath: '../dist/rc/prod/',
    jsonpFunction:'ReactComponents'
  },

  resolve: {
    modules: ['./node_modules'],
    extensions: ['.js', '.jsx', '.json', '.css', '.styl'],
    alias: {
      '~': resolve('src'),
      conf: resolve('conf'),
    },
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: resolve('.'),
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract({
          fallback: require.resolve('style-loader'),
          use: [
            {
              loader: require.resolve('css-loader'),
              options: {
                importLoaders: 1,
                minimize: true,
                modules: true,
                localIdentName: '[hash:base64:4]',
              },
            },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                ident: 'postcss',
                plugins: () => [
                  require('postcss-cssnext'),
                  require('postcss-assets'),
                  require('postcss-flexbugs-fixes'),
                ],
              },
            },
            {
              loader: require.resolve('stylus-loader'),
            },
          ],
        }),
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: require.resolve('css-loader'),
            options: {
              modules: false,
            },
          },
        ],
      },
    ],
  },

  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    'react-dom/server': 'ReactDOMServer',
  },

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.HashedModuleIdsPlugin(),

    new webpack.EnvironmentPlugin({
        NODE_ENV: process.env.NODE_ENV,
        BABEL_ENV: process.env.BABEL_ENV,
    }),

    new ExtractTextPlugin({
      filename: fileNamePatern + '.css',
      allChunks: true,
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'widget-common',
    }),

    new UglifyJsPlugin({
      uglifyOptions: {
        ie8: false,
        ecma: 5,
        output: {
          comments: false,
          beautify: false,
        },
        compress: {
          sequences: true,
          booleans: true,
          loops: true,
          unused: true,
          warnings: false,
          drop_console: true
        },
        warnings: false,
      },
    }),

    new ManifestPlugin({
      fileName: 'rev.json'
    }),
  ],

  stats: {
    assets: true,
    cached: true,
    errors: true,
    errorDetails: true,
    hash: true,
    timings: true,
    version: true,
    warnings: true,
  },
}
