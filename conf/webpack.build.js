import webpack from 'webpack'
import { resolve, join } from 'path'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import ManifestPlugin from 'webpack-manifest-plugin'
import CompressionPlugin from 'compression-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import autoprefixer from 'autoprefixer'

process.env.NODE_ENV = 'production'
process.env.BABEL_ENV = 'production'

const outputPath = join(__dirname, '../../dist/dev/')
const fileNamePatern = '[name]'

export default {
  context: resolve('src'),

  entry: {
    'widget-common': ['react-hot-loader/patch', 'babel-polyfill', './utils/initialize.js'],
    'widget-calc': ['./entries/calc'],
  },

  output: {
    filename: fileNamePatern + '.js',
    pathinfo: true,
    path: outputPath,
    publicPath: '../dist/rc/dev/',
    jsonpFunction:'ReactComponents'
  },

  resolve: {
    modules: ['./node_modules'],
    extensions: ['.js', '.jsx', '.json', '.css', '.styl'],
    alias: {
      '~': resolve('src'),
      conf: resolve('conf'),
    },
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: resolve('.'),
        exclude: /node_modules/,
        loader: require.resolve('babel-loader'),
        options: {
          compact: true,
        },
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract({
          fallback: require.resolve('style-loader'),
          use: [
            {
              loader: require.resolve('css-loader'),
              options: {
                sourceMap: true,
                importLoaders: 1,
                minimize: false,
                modules: true,
                localIdentName: '[local]--[hash:base64:4]',
              },
            },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                sourceMap: true,
                ident: 'postcss',
                plugins: () => [
                  require('postcss-cssnext'),
                  require('postcss-assets'),
                  require('postcss-flexbugs-fixes'),
                ],
              },
            },
            {
              loader: require.resolve('stylus-loader'),
              options: {
                sourceMap: true,
              },
            },
          ],
        }),
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: require.resolve('css-loader'),
            options: {
              modules: false,
            },
          },
        ],
      },
    ],
  },

  devtool: 'inline-source-map',

  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    'react-dom/server': 'ReactDOMServer',
  },

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.HashedModuleIdsPlugin(),

    new ExtractTextPlugin({
      filename: fileNamePatern + '.css',
      allChunks: true,
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'widget-common',
    }),

    new webpack.EnvironmentPlugin({
      NODE_ENV: process.env.NODE_ENV,
      BABEL_ENV: process.env.BABEL_ENV,
    }),

    new ManifestPlugin({
      fileName: 'rev.json'
    }),
  ],
}
