module.exports = () => {
  return {
    plugins: [require('postcss-cssnext'), require('postcss-assets')],
  }
}
