import calc from 'conf/app/partials/calc'
import dev from 'conf/app/partials/development'
import prod from 'conf/app/partials/production'

const conf = {
  development: {
    ...dev,
    calc
  },
  production: {
    ...prod,
    calc
  },
}

export default conf[process.env.NODE_ENV]
