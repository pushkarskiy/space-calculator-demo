export default {
  apiUrlGetObjects: 'http://localhost:4001/get-objects',
  apiUrlFilters: 'http://localhost:4001/get-filter',
  searchUrl: 'http://localhost:4001/search'
}