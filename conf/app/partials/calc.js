export default {
  roomsList: ['meetingRooms', 'offices', 'kitchen'],
  employees: {
    value: 10,
    min: 0,
    max: 2000,
  },
  meetingRooms: {
    value: 0,
    min: 0,
    max: 100,
    default: {
      0: 1,
      100: 3,
      250: 6,
      500: 20,
    },
  },
  offices: {
    value: 0,
    min: 0,
    max: 100,
    default: {
      0: 1,
      100: 5,
      250: 10,
      500: 25,
    },
  },
  kitchen: {
    value: 0,
    min: 0,
    max: 100,
    default: {
      0: 0,
      100: 1,
      250: 4,
      500: 10,
    },
  },

  orderTarget: '#order',

  areaForOnePerson: {
    value: 3.5,
    options: [
      {
        value: 3.5,
        label: '3,5 - плотная',
      },
      {
        value: 5,
        label: '5 - свободная',
      },
      {
        value: 7,
        label: '7 - просторная',
      },
    ],
  },
  areaForOneMeetingRoom: {
    value: 25,
  },
  areaForOneOffice: {
    value: 15,
  },
  areaForOneKitchen: {
    value: 30,
  },
}
