import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import Calc from '~/modules/Calculator'
import SpaceCalculator from '~/containers/SpaceCalculator'
import Config from '~/containers/Config'
import Initializer from '~/utils/initialize'

function init(container, props) {
  let app = (
    <Config config={props.config} appId={props.appId}>
      <SpaceCalculator>
        <Calc />
      </SpaceCalculator>
    </Config>
  )

  if (module.hot) {
    app = (
      <AppContainer>
        <Config config={props.config} appId={props.appId}>
          <SpaceCalculator>
            <Calc />
          </SpaceCalculator>
        </Config>
      </AppContainer>
    )
  }

  render(app, container)
}

Initializer.registration('calc', init)