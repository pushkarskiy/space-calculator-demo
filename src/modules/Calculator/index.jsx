import jump from 'jump.js'
import qs from 'query-string'
import isEqual from 'lodash.isequal'
import debounce from 'lodash.debounce'
import React, { Component } from 'react'
import autobind from 'autobind-decorator'

import styles from './styles'
import API, { buildUrl } from '~/api'
import { easeInOutQuad } from '~/utils/easing'

import Icon from '~/components/Icon'
import Chart from '~/components/Chart'
import Legend from '~/components/Legend'
import Button from '~/components/Button'
import Tooltip from '~/components/Tooltip'
import ButtonLink from '~/components/ButtonLink'
import InputGroup from '~/components/InputGroup'
import InputNumber from '~/components/InputNumber'
import LineSeparator from '~/components/LineSeparator'
import { Col, Row, Container } from '~/components/Grid'
import FilterCustomSelect from '~/components/FilterCustomSelect'
import { withConfig } from '~/containers/Config'

@withConfig
class Calculator extends Component {
  constructor(props) {
    super(props)
    this.state = { data: {} }
    this.init()
  }

  init() {
    const { totalArea } = this.props
    const query = this.getQuery(totalArea)

    this.fetchObjects(query)
  }

  componentWillReceiveProps(nextProps) {
    const { totalArea } = this.props

    if (totalArea !== nextProps.totalArea) {
      this.fetchObjects(this.getQuery(nextProps.totalArea))
    }
  }

  render() {
    const { data = {} } = this.state

    const {
      calc,
      decrement,
      increment,
      updateField,
      updateEmployees,

      orderTarget,

      offices = {},
      kitchen = {},
      employees = {},
      meetingRooms = {},
      areaForOnePerson = {},

      totalArea,
      totalKitchen,
      totalOffices,
      totalOpenSpace,
      totalMeetingRooms,
    } = this.props

    const area = {
      totalKitchen,
      totalOffices,
      totalOpenSpace,
      totalMeetingRooms,
    }
    
    const employeesTooltip = employees.value <= 10 ? 'show' : ''

    return (
      <Container>
        <Row>
          <Col xs="12" sm="12" md="12" lg="6" className={styles['calc__left']}>
            <h2>Калькулятор офисных пространств</h2>
            <Row>
              <Col xs="12" sm="12" md="12" lg="12" xl="6">
                <InputGroup label="Количество сотрудников:">
                  <Tooltip text="Укажите количество сотрудников" mod={employeesTooltip}>
                    <InputNumber value={employees.value} name="employees" setField={updateEmployees} autoFocus={true} />
                  </Tooltip>
                </InputGroup>
              </Col>
              <Col xs="12" sm="12" md="12" lg="12" xl="6">
                <InputGroup label="Площадь кв. м на 1 сотрудника:">
                  <FilterCustomSelect {...areaForOnePerson} onChange={updateField} name="areaForOnePerson" />
                </InputGroup>
              </Col>
            </Row>

            <h3>Рекомендуемые параметры:</h3>
            <Row>
              <Col xs="12" sm="12" md="4" lg="4">
                <InputGroup label="Кабинеты:">
                  <InputNumber
                    value={offices.value}
                    name="offices"
                    setField={updateField}
                    increment={increment}
                    decrement={decrement}
                  />
                </InputGroup>
              </Col>

              <Col xs="12" sm="12" md="4" lg="4">
                <InputGroup label="Переговорные:">
                  <InputNumber
                    value={this.props.meetingRooms.value}
                    name="meetingRooms"
                    setField={updateField}
                    increment={increment}
                    decrement={decrement}
                  />
                </InputGroup>
              </Col>

              <Col xs="12" sm="12" md="4" lg="4">
                <InputGroup label="Кухня:">
                  <InputNumber
                    value={kitchen.value}
                    name="kitchen"
                    setField={updateField}
                    increment={increment}
                    decrement={decrement}
                  />
                </InputGroup>
              </Col>
            </Row>
          </Col>
          <Col xs="12" sm="12" md="12" lg="6" className={styles['calc__right']}>
            <Chart area={area} total={totalArea}>
              {employees.value > 0
                ? <Legend label={`Опен спейс: на ${employees.value} чел.`} total={totalOpenSpace} mod="blue" />
                : null}
              {offices.value > 0
                ? <Legend label={`Кабинеты: ${offices.value}`} total={totalOffices} mod="red" />
                : null}
              {meetingRooms.value > 0
                ? <Legend label={`Переговорные: ${meetingRooms.value}`} total={totalMeetingRooms} mod="orange" />
                : null}
              {kitchen.value > 0 ? <Legend label={`Кухня: ${kitchen.value}`} total={totalKitchen} mod="green" /> : null}
              <div className={styles['calc__action']}>
                {orderTarget && orderTarget !== ''
                  ? <Button
                    onClick={this.handleOnOrderClick.bind(this)}
                    name="order"
                    label="Оставить заявку"
                    lightBlue
                  />
                  : null}
              </div>
            </Chart>
          </Col>
        </Row>
        <Row>
          <Col>
            {data.rents || data.sales ? this.getHeaderGrid() : null}
          </Col>
        </Row>
      </Container>
    )
  }

  // Templates
  @autobind
  getHeaderGrid() {
    const { data } = this.state
    return (
      <Container>
        <Row className={styles['calc__catalog_title']}>
          <Col xs="12" sm="12" md="12" lg="4">
            <h2>Подходящие предложения:</h2>
          </Col>

          <Col xs="12" sm="12" md="6" lg="4">
            {data.sales ? this.getBtnForSale() : null}
          </Col>

          <Col xs="12" sm="12" md="6" lg="4">
            {data.rents ? this.getBtnForRent() : null}
          </Col>
        </Row>
      </Container>
    )
  }

  @autobind
  getBtnForSale() {
    const { data } = this.state
    const { totalArea } = this.props
    const query = { ...this.getQuery(totalArea), typeListing: 'sale' }
    const url = this.buildUrl(query)

    return <ButtonLink href={url} target="_blank" label={`Офисы на продажу (${data.sales})`} lightBlue marginBottom />
  }

  @autobind
  getBtnForRent() {
    const { data } = this.state
    const { totalArea } = this.props
    const query = { ...this.getQuery(totalArea), typeListing: 'rent' }
    const url = this.buildUrl(query)

    return <ButtonLink href={url} target="_blank" label={`Офисы в аренду (${data.rents})`} lightBlue marginBottom />
  }

  @autobind
  handleOnOrderClick = e => {
    e.preventDefault()
    jump(this.props.orderTarget, {
      duration: 1000,
      offset: 100,
      easing: easeInOutQuad,
    })
  }

  @autobind
  fetchObjects(query = {}) {
    API.getObjects(this.props.config.apiUrlGetObjects, query).then(({ success, data }) => {
      success ? this.setState({ data }) : console.warn('Objects not found')
    })
  }

  @autobind
  getQuery(total) {
    const query = { type: 'office' }

    if (!total) return query

    const delta = Math.floor(parseFloat(total) / 100 * 10)

    query.areaTo = total + delta
    query.areaFrom = total - delta

    return query
  }

  @autobind
  buildUrl(query) {
    const search = qs.stringify(query)
    return `${this.props.config.searchUrl}?${search}`
  }
}

export default Calculator
