import Config from './Config';
import withConfig from './withConfig';

export {
    Config as default,
    withConfig,
}
