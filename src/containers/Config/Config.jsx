import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import PropTypes from 'prop-types'
import assignDeep from 'assign-deep'

import conf from 'conf/app'

export default class Config extends Component {
  constructor(props) {
    super(props)
    this.state = assignDeep({}, conf, props.config)
  }

  static childContextTypes = {
    getConfig: PropTypes.func
  }

  getChildContext() {
    return {
      getConfig: this.getConfig
    }
  }

  render() {
    return React.cloneElement(this.props.children)
  }

  @autobind
  getConfig() {
    return this.state
  }
}
