import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default (WrappedComponent) => (
  class withConfig extends Component {
    static contextTypes = {
      getConfig: PropTypes.func
    }

    render() {
      const props = { ...this.props, config: this.context.getConfig() }

      return <WrappedComponent {...props}>{this.props.children}</WrappedComponent>
    }
  }
)
