import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import PropTypes from 'prop-types'
import { withConfig } from '~/containers/Config'

@withConfig
export default class SpaceCalculator extends Component {
  constructor(props) {
    super(props)
    this.state = this.init(props.config.calc)
  }

  init(state = {}) {
    return {
      ...state,
      ...this.updateAreas(state),
    }
  }

  render() {
    const { children } = this.props

    const props = {
      ...this.state,
      ...children.props,
      decrement: this.decrement,
      increment: this.increment,
      updateField: this.updateField,
      updateEmployees: this.updateEmployees,
    }

    return React.cloneElement(children, props)
  }

  @autobind
  updateEmployees(name, value) {
    if (!name) return

    const newValue = !value ? 0 : value
    const rooms = this.props.config.calc.roomsList

    let state = Object.assign({}, this.state)
    state = Object.assign({}, state, this.combainFieldState(state, name, newValue))
    state = Object.assign({}, state, this.updateDefaultValuesByRooms(state, rooms, newValue))
    state = Object.assign({}, state, this.updateAreas(state))

    this.setState(() => state)
  }

  @autobind
  updateDefaultValuesByRooms(prevState, rooms, employees) {
    const state = { ...prevState }

    for (let room of rooms) {
      const item = state[room]
      const value = this.checkDefaulValue(item.default, item.value, employees)
      state[room] = Object.assign({}, state[room], { value })
    }

    return state
  }

  @autobind
  updateField(name, value) {
    if (!name) return

    const newValue = !value ? 0 : value

    let state = Object.assign({}, this.state)
    state = Object.assign({}, state, this.combainFieldState(state, name, newValue))
    state = Object.assign({}, state, this.updateAreas(state))

    this.setState(() => state)
  }

  @autobind
  increment(name) {
    if (!name) return

    const prevState = this.state
    const newValue = prevState[name].value + 1

    let state = Object.assign({}, prevState)
    state = Object.assign({}, state, this.combainFieldState(state, name, newValue))
    state = Object.assign({}, state, this.updateAreas(state))

    this.setState(() => state)
  }

  @autobind
  decrement(name) {
    if (!name) return

    const prevState = this.state
    const newValue = this.checkMinValue(prevState[name].value - 1)

    let state = Object.assign({}, prevState)
    state = Object.assign({}, state, this.combainFieldState(state, name, newValue))
    state = Object.assign({}, state, this.updateAreas(state))

    this.setState(() => state)
  }

  @autobind
  combainFieldState(state, name, value) {
    return {
      [name]: {
        ...state[name],
        value: value,
      },
    }
  }

  @autobind
  updateAreas(state = {}) {
    const {
      offices,
      kitchen,
      employees,
      meetingRooms,
      areaForOnePerson,
      areaForOneOffice,
      areaForOneKitchen,
      areaForOneMeetingRoom,
    } = state

    let total = 0

    const areas = {
      totalOffices: offices.value * parseFloat(areaForOneOffice.value),
      totalKitchen: kitchen.value * parseFloat(areaForOneKitchen.value),
      totalOpenSpace: employees.value * parseFloat(areaForOnePerson.value),
      totalMeetingRooms: meetingRooms.value * parseFloat(areaForOneMeetingRoom.value),
    }

    for (let value in areas) total += areas[value]

    areas.totalArea = total

    return areas
  }

  checkDefaulValue(values = {}, value, employees) {
    const keys = Object.keys(values)
    let result = value

    for (let i = 0; i < keys.length; i++) {
      if (employees > parseInt(keys[i], 10)) {
        result = values[keys[i]]
      }
    }

    return result
  }

  checkMinValue(value, min = 0) {
    return value <= min ? min : value
  }

  static checkValue(value, min, max) {
    return value > max ? max : value < min ? min : value
  }
}
