import React, { Component } from 'react'

class DocumentResize extends Component {
  constructor () {
    super()

    this.state = {
      show: true
    }
  }

  componentDidMount () {
    this.handleOnResize()
    window.addEventListener('resize', this.handleOnResize.bind(this))
  }

  render () {
    const { children } = this.props
    const { show } = this.state
    return show ? children : null
  }

  handleOnResize () {
    const { minWidth, onResizeCallback } = this.props
    let width = document.body.clientWidth
    if (width < minWidth) {
      this.setState(() => ({ show: false }))
      if (onResizeCallback && typeof onResizeCallback === 'function') {
        onResizeCallback()
      }
    } else {
      this.setState(() => ({ show: true }))
    }
  }
}

export default DocumentResize
