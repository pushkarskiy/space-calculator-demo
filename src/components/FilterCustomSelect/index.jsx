import React, { Component }  from 'react'
import classNames from 'classnames/bind'
import device from '~/utils/device'

import InputGroup from '~/components/InputGroup'
import FilterMultiSelectItem from '~/components/FilterMultiSelectItem'
import DropDown from '~/components/DropDown'
import Select from '~/components/Select'

import cx from './styles'

class FilterCustomSelect extends Component {

    render() {

        let classes = [cx['customSelect']]
        
        return (
            <div className={classes.join(' ')}>
                { device.desktop() ? this.getCustomSelect() : this.getSelect()}
            </div>
        )
    }

    // Templates
    getSelect() {
        const { label } = this.props;
        return (
            <InputGroup label={label} >
                <Select {...this.props} />
            </InputGroup>
        )
    }

    getCustomSelect() {
        const { label, options, value, hideLabel } = this.props;
        return (
            <DropDown
                trigger={ this.getSelectTrigger()}
                label={label}
                bodyWrapperNoPadding
                closeByClickItem
                hideLabel = {hideLabel}
                children = {options.map((item, index) => {
                    return (
                        <FilterMultiSelectItem
                            {...item}
                            checked={value === item.value}
                            key={index}
                            onChange={this.handleOnChange.bind(this)}
                        />
                    )
                })}
            />
        )
    }

    getSelectTrigger() {
        const { options, value } = this.props;
        const _options = options.filter(item => value === item.value);
        const label = _options.lenght && _options[0].label ? _options[0].label : "";
        let classes = [cx['customSelect__trigger']];
        
        return (
            <div className={classes.join(' ')}>
                <span dangerouslySetInnerHTML={{ __html: _options[0].label }} />
            </div>
        )
    }

    // Events
    handleOnChange(value) {
        const { onChange, name } = this.props;
        if (onChange) onChange(name, value);
    }
}

export default FilterCustomSelect