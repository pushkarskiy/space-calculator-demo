import React, { Component } from 'react'
import classNames from 'classnames/bind'
import cx from './styles'

import InputGroup from '~/components/InputGroup'
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'

class DropDown extends Component {
  constructor () {
    super()
    this.state = { show: false }
  }

  render () {
    const { label, trigger, bodyWrapperNoPadding, hideLabel } = this.props

    let classesBody = [cx['dropDown__body']]
    if (this.state.show) classesBody.push(cx['dropDown__body_has_open'])

    let classesBodyWrapper = [cx['dropDown__bodyWrapper']]
    if (bodyWrapperNoPadding) { classesBodyWrapper.push(cx['dropDown__bodyWrapper_no_padding']) }

    let classesTrigger = [cx['dropDown__trigger']]
    if (this.state.show) classesTrigger.push(cx['dropDown__trigger_has_open'])

    return (
      <Dropdown
        className={cx['dropDown']}
        isOpen={this.state.show}
        toggle={this.handleOnClickTrigger.bind(this)}
      >
        <InputGroup label={label} hideLabel={hideLabel}>
          <DropdownToggle className={classesTrigger.join(' ')}>
            {trigger}
          </DropdownToggle>
        </InputGroup>

        <DropdownMenu className={classesBody.join(' ')}>
          <div className={classesBodyWrapper.join(' ')}>
            <div onClick={this.handleCloseByClickItem.bind(this)}>
              {this.props.children}
            </div>
          </div>
        </DropdownMenu>
      </Dropdown>
    )
  }

  // Events
  handleOnClickTrigger () {
    this.setState(() => ({ show: !this.state.show }))
  }

  handleCloseByClickItem () {
    this.props.closeByClickItem &&
      this.setState(() => ({ show: !this.state.show }))
  }
}

export default DropDown
