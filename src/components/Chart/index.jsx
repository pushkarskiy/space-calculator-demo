import React, { Component } from 'react'
import Donut from '~/components/Donut'
import { Col, Row, Container } from '~/components/Grid'

import { chart, chart__caption, chart__total } from './styles'

class Chart extends Component {
  render () {
    const { area, total, children } = this.props

    return (
      <div>
        <div className={chart__caption}>
          <h2>
            Рекомендуемая площадь М<sup>2</sup>:
          </h2>
          <div className={chart__total}>~ {total}</div>
        </div>
        <Row className={chart}>
          <Col xs="12" sm="12" md="5" lg="5">
            <Donut area={area} />
          </Col>
          <Col xs="12" sm="12" md="7" lg="7">
            {children}
          </Col>
        </Row>
      </div>
    )
  }
}

export default Chart
