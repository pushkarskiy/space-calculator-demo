import React, { Component } from 'react'
import cx from './styles'

export class Td extends Component {
  render () {
    const {
      children,
      md,
      sm,
      xs,
      lg,
      xl,
      className,
      direction,
      th,
      colSpan,
      onClick,
      sizeMin
    } = this.props

    let classes = [cx['td']]
    if (className) classes.push(className)
    if (xl) classes.push(cx[`td-xl-${xl}`])
    if (lg) classes.push(cx[`td-lg-${lg}`])
    if (md) classes.push(cx[`td-md-${md}`])
    if (sm) classes.push(cx[`td-sm-${sm}`])
    if (xs) classes.push(cx[`td-xs-${xs}`])
    if (th) classes.push(cx[`th`])
    if (sizeMin === 'xs') classes.push(cx[`td_xs`])
    if (sizeMin === 's') classes.push(cx[`td_s`])
    if (sizeMin === 'm') classes.push(cx[`td_m`])
    if (sizeMin === 'l') classes.push(cx[`td_l`])
    if (onClick) classes.push(cx[`th-action`])
    if (direction) classes.push(cx[`th-direction-${direction}`])

    return (
      <td
        className={classes.join(' ')}
        onClick={this.handleOnclick.bind(this)}
        colSpan={colSpan}
      >
        <span>{children}</span>
      </td>
    )
  }

  handleOnclick () {
    const { onClick } = this.props
    if (onClick) onClick()
  }
}

export class Tr extends Component {
  render () {
    const { children, className, dark } = this.props

    let classes = [cx['tr']]
    if (className) classes.push(className)
    if (dark) classes.push(cx[`tr_dark`])

    return <tr className={classes.join(' ')}>{children}</tr>
  }
}

export class Thead extends Component {
  render () {
    const { children, className } = this.props

    let classes = [cx['thead']]
    if (className) classes.push(className)

    return <thead className={classes.join(' ')}>{children}</thead>
  }
}

export class Tbody extends Component {
  render () {
    const { children, className } = this.props

    let classes = [cx['tbody']]
    if (className) classes.push(className)

    return <tbody className={classes.join(' ')}>{children}</tbody>
  }
}

export class Table extends Component {
  render () {
    const { children, className, responsive } = this.props

    let classes = [cx['table']]
    if (className) classes.push(className)
    if (responsive) classes.push(cx['table_responsive'])

    return <table className={classes.join(' ')}>{children}</table>
  }
}

export default Table
