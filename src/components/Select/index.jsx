import React, { Component, PropTypes } from 'react'
import getClassName from '~/utils/getClassName'
import style from './styles'

class Select extends Component {
  render() {
    const { options, mod, value, name } = this.props
    return (
      <div className={getClassName('field', mod, style)}>
        <select
          name={name}
          value={value}
          onChange={this.handleOnChange.bind(this)}
        >
          {options.map((item, i) => {
            return (
              <option value={item.value} key={i}>
                {item.label}
              </option>
            )
          })}
        </select>
      </div>
    )
  }

  handleOnChange = e => {
    e.preventDefault()
    this.props.onChange(e.target.name, e.target.value)
  }
}

export default Select
