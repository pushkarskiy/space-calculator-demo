import React, { Component } from 'react'
import styles from './styles'

class ButtonLink extends Component {
  render() {
    const {
      label,
      href,
      target,

      name,
      light,
      white,
      inline,
      onClick,
      withIcon,
      disabled,
      children,
      widthAuto,
      lightBlue,
      redSearch,
      marginBottom,
    } = this.props

    let classes = [styles['button']]
    if (lightBlue) classes.push(styles['light_blue'])
    if (white) classes.push(styles['white'])
    if (widthAuto) classes.push(styles['width_auto'])
    if (marginBottom) classes.push(styles['margin_bottom'])
    if (light) classes.push(styles['light'])
    if (withIcon) classes.push(styles['with_icon'])
    if (inline) classes.push(styles['inline'])
    if (redSearch) classes.push(styles['red_search'])
    if (disabled) classes.push(styles['disabled'])

    return (
      <a
        className={classes.join(' ')}
        href={href}
        onClick={this.handleOnClickDisableButton.bind(this, disabled)}
        target={target}>
        <span>
          {label}
        </span>
      </a>
    )
  }

  handleOnClickDisableButton(disabled, e) {
    if (disabled) e.preventDefault()
  }
}

export default ButtonLink
