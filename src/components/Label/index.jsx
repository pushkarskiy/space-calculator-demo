import React, {Component}  from 'react'
import getClassName from '~/utils/getClassName'
import style from './styles'

class Label extends Component {

    render() {
        const {children, mod} = this.props;
        return (<label className={getClassName('label', mod, style)}>{children}</label>)
    }
}

export default Label