import React, {Component}  from 'react'
import cx from './styles'

export class Col extends Component {

    render() {
        const { children, md, sm, xs, lg, xl, className, max} = this.props;

        let prefix = '';
        let classes = [cx[`${prefix}col`]];
        
        if (max) prefix = 'max-';
        if (className) classes.push(className);
        if (xl) classes.push(cx[`${prefix}col-xl-${xl}`]);
        if (lg) classes.push(cx[`${prefix}col-lg-${lg}`]);
        if (md) classes.push(cx[`${prefix}col-md-${md}`]);
        if (sm) classes.push(cx[`${prefix}col-sm-${sm}`]);
        if (xs) classes.push(cx[`${prefix}col-xs-${xs}`]);
        
        return (
           <div className={classes.join(' ')}>
               {children}
           </div>
        )
    }
}

export class Row extends Component {

    render() {
        const { children, className } = this.props;

        let classes = [cx['row']];
        if (className) classes.push(className);

        return (
           <div className={classes.join(' ')}>
               {children}
           </div>
        )
    }
}

export class Container extends Component {

    render() {
        const { children, className } = this.props;

        let classes = [cx['container']];
        if (className) classes.push(className);

        return (
           <div className={classes.join(' ')}>
               {children}
           </div>
        )
    }
}

export default Container;