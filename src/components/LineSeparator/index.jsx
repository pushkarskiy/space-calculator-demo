import React, {Component}  from 'react'
import {Col, Row, Container} from '~/components/Grid';
import cx from './styles'

class LineSeparator extends Component {
    render() {
        return (
            <Col xs="12" sm="12" md="12" lg="12" xl="12" className={cx['line-separator']}>
                <div className={cx['line-separator__wrapper']}>
                    <div className={cx['line-separator__children']}>
                        {this.props.children}
                    </div>
                </div>
            </Col>
        )
    }
}

export default LineSeparator