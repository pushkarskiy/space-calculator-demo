import React, { Component }  from 'react'
import wNumb from 'wnumb'

import style from './styles'

class InputNumber extends Component {

    constructor() {
        super();
        this.numberFormat = wNumb({
            decimals: 0,
            thousand: ' '
        });
    }

    render() {
        const { mod, value, autoFocus, increment, decrement } = this.props;
        let className                                         = style[ mod ] ? `${style.field} ${style[ mod ]}` : style.field;

        return (
            <div className={className}>
                {decrement ? this.getDecrement() : null}
                <input
                    type="text"
                    value={this.numberFormat.to(typeof value === 'string' ? Number(value) : value)}
                    onChange={this.onChange.bind(this)}
                    onKeyPress={this.onKeyPress.bind(this)}
                    autoFocus={autoFocus}
                />
                {increment ? this.getIncrement() : null}
            </div>
        )
    }

    getIncrement() {
        return (
            <div
                onClick={this.onClickPlus.bind(this)}
                className={`${style.btn} ${style.plus}`}>
                +
            </div>
        )
    }

    getDecrement() {
        return (
            <div
                onClick={this.onClickMinus.bind(this)}
                className={`${style.btn} ${style.minus}`}>
                -
            </div>
        )
    }

    onClickPlus = e => {
        e.preventDefault();
        const { name, increment } = this.props;
        increment(name);
    };

    onClickMinus = e => {
        const { name, decrement } = this.props;
        decrement(name);
    };

    onChange = e => {
        e.preventDefault();
        let value                = this.numberFormat.from(e.target.value);
        const { name, setField } = this.props;

        value > 0
            ? setField(name, value)
            : setField(name, 0);
    };

    onKeyPress = e => {
        let chr = String.fromCharCode((e.which) ? e.which : e.charCode);

        if (chr < '0' || chr > '9') {
            e.preventDefault();
        }
    };

}

export default InputNumber