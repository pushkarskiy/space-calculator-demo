import React, { Component } from 'react'
import getClassName from '~/utils/getClassName'
import cx from './styles'

class Button extends Component {
  render() {
    const {
      label,
      name,
      onClick,
      children,
      lightBlue,
      white,
      widthAuto,
      marginBottom,
      theme_collapsed_button,
      inline,
      redSearch,
      disabled,
      light,
      withIcon,
    } = this.props

    let classes = [cx['button']]
    if (lightBlue) classes.push(cx['light_blue'])
    if (white) classes.push(cx['white'])
    if (widthAuto) classes.push(cx['width_auto'])
    if (marginBottom) classes.push(cx['margin_bottom'])
    if (light) classes.push(cx['light'])
    if (withIcon) classes.push(cx['with_icon'])
    if (inline) classes.push(cx['inline'])
    if (redSearch) classes.push(cx['red_search'])
    if (disabled) classes.push(cx['disabled'])

    return (
      <button className={classes.join(' ')} onClick={onClick} name={name} disabled={disabled}>
        {children ||
          <span>
            {label}
          </span>}
      </button>
    )
  }
}

export default Button
