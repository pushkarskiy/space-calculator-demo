import React, { Component } from 'react'
import classNames from 'classnames/bind'
import style from './styles'

class Title extends Component {
  constructor () {
    super()
    this.cx = classNames.bind(style)
  }

  render () {
    const { children, mod } = this.props

    let titleCX = this.cx({
      title: true,
      title__align_left: mod === 'align_left'
    })

    return <div className={titleCX}>{children}</div>
  }
}

export default Title
