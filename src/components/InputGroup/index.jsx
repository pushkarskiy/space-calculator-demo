import React, {Component}  from 'react'
import Label from '~/components/Label'
import getClassName from '~/utils/getClassName'
import cx from './styles'

class InputGroup extends Component {

    render() {
        const {label, children, widthAuto, hideLabel, labelEmpty} = this.props;
        
        let classes = [cx['group']]
        if (labelEmpty) classes.push(cx['label_empty'])
        if (widthAuto) classes.push(cx['width_auto'])

        return (
            <div className={classes.join(' ')}>
                {label && !hideLabel ? <Label>{label}</Label> : ''}
                {children}
            </div>
        )
    }
}

export default InputGroup