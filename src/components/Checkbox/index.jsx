import React, { Component } from 'react'
import {
  checkbox,
  checkbox__label,
  checkbox__icon,
  checkbox__icon_checked
} from './styles'

class Checkbox extends Component {
  render () {
    const checkboxIconClassName = this.props.checked
      ? `${checkbox__icon} ${checkbox__icon_checked}`
      : `${checkbox__icon}`

    return (
      <div className={checkbox} onClick={this.handleOnClick.bind(this)}>
        <div className={checkboxIconClassName} />
        <label className={checkbox__label}>{this.props.label}</label>
      </div>
    )
  }

  handleOnClick () {
    this.props.onChange(this.props.value)
  }
}

export default Checkbox
