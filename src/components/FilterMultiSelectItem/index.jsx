import React, { Component }  from 'react'
import classNames from 'classnames/bind'
import cx from './styles'

class FilterMultiSelectItem extends Component {

    render() {

        let classes = [cx['multiSelectItem']]
        if (!!this.props.checked) classes.push(cx['multiSelectItem_checked'])

        return (
            <div className={classes.join(' ')} onClick={this.handleOnClick.bind(this)}>
                <label className={cx['multiSelectItem__label']} dangerouslySetInnerHTML={{ __html: this.props.label }} />
            </div>
        )
    }

    handleOnClick() {
        this.props.onChange(this.props.value);
    }
}

export default FilterMultiSelectItem