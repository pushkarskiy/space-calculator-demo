import React, { Component } from 'react'
import getClassName from '~/utils/getClassName'
import style from './styles'

class Tooltip extends Component {
  render () {
    const { mod, text, children } = this.props
    return (
      <div className={getClassName('field', mod, style)}>
        <span className={style.hint}>{text}</span>
        {children}
      </div>
    )
  }
}

export default Tooltip
