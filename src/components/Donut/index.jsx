import React, { Component } from 'react'
import { Doughnut } from 'react-chartjs-2'
import style from './styles'

class Donut extends Component {
  render () {
    let data = {
      datasets: [
        {
          data: this.getValue(),
          backgroundColor: [
            '#0e4269',
            '#e40613',
            '#f58e01',
            '#3baa34',
            '#009fe3',
            '#946038',
            '#e41973'
          ],
          borderWidth: 0
        }
      ]
    }

    let options = {
      cutoutPercentage: 60,
      tooltips: {
        enabled: false
      }
    }

    return (
      <div className={style.donut}>
        <Doughnut data={data} width={100} height={100} options={options} />
      </div>
    )
  }

  getValue () {
    let result = []
    const { area } = this.props
    for (let key in area) {
      result.push(area[key])
    }
    return result
  }
}

export default Donut
