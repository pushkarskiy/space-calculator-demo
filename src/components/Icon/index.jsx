import React, { Component }  from 'react'
import getClassName from '~/utils/getClassName'
import style from './styles'

class Icon extends Component {
    render() {
        const { mod, title } = this.props;
        return (
            <i className={getClassName('ic', mod, style)} title = { title}/>
        )
    }
}

export default Icon