import React, { Component }  from 'react'
import classNames from 'classnames/bind'
import style from './styles'

class Input extends Component {

    constructor() {
        super();
        this.cx = classNames.bind(style);
    }

    render() {
        const { value, name, autoFocus, required, placeholder, simple } = this.props;

        let inputTextCX = this.cx({
            inputText: true,
            inputText__simple: !!simple,
        });

        return (
            <input
                name={name}
                className={inputTextCX}
                type="text"
                value={value}
                onChange={this.onChange.bind(this)}
                onKeyPress={this.onKeyPress.bind(this)}
                onPaste={this.onPaste.bind(this)}
                autoFocus={autoFocus}
                required={required}
                placeholder={placeholder}
                autoComplete="false"
            />
        )
    }

    onKeyPress = e => {
        if (this.props.type === 'number') {
            let chr = String.fromCharCode((e.which)
                ? e.which
                : event.charCode);

            if (chr < '0' || chr > '9') {
                e.preventDefault();
            }
        }
    };

    onChange = e => {
        e.preventDefault();
        typeof this.props.onChange ==='function' && this.props.onChange(e.target.name, e.target.value)
    };

    onPaste = e => {
        e.preventDefault();
    };
}

export default Input