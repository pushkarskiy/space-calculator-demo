import React, {Component}  from 'react'
import getClassName from '~/utils/getClassName'
import style, {
    legend,
    legend__label,
    legend__total,
} from './styles'

class DonutInfoItem extends Component {
    render() {
        const {label, total, mod} = this.props;

        return (
            <div className={legend}>
                <label className={legend__label}>{label}</label>
                <b className={getClassName('legend__total', mod, style)}>{total} м<sup>2</sup></b>
            </div>
        )
    }


}

export default DonutInfoItem