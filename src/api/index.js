import fetchJSONP from '~/utils/fetchJSONP'
import qs from 'query-string'

export function getObjects(url, query) {
  return fetchJSONP(buildUrl(url, query))
}

export function buildUrl(pathname, query = {}) {
  const search = qs.stringify(query)

  return search.length ? `${pathname}?${search}` : pathname
}

export default { getObjects }
