import device from 'device.js'

export function getDeviceType () {
  if (device().desktop()) {
    return 'desktop'
  }
  if (device().tablet()) {
    return 'tablet'
  }
  if (device().mobile()) {
    return 'tablet'
  }
}

export function getOS () {
  if (device().ios()) {
    return 'ios'
  }
  if (device().ipad()) {
    return 'ipad'
  }
  if (device().android()) {
    return 'android'
  }
  if (device().windows()) {
    return 'windows'
  }
  if (device().windowsPhone()) {
    return 'windowsPhone'
  }
  if (device().windowsTablet()) {
    return 'windowsTablet'
  }
}

export default device()
