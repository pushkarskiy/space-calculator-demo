import wNumb from 'wnumb'

const FORMAT = {
  thousand: {
    ruble: {
      decimals: 0,
      thousand: ' ',
      suffix: ' тыс. руб'
    },
    dollar: {
      prefix: '$ ',
      decimals: 0,
      thousand: ' ',
      suffix: 'K'
    }
  },
  million: {
    ruble: {
      decimals: 0,
      thousand: ' ',
      suffix: ' млн. руб'
    },
    dollar: {
      prefix: '$ ',
      decimals: 0,
      thousand: ' ',
      suffix: 'M'
    }
  }
}

export default function roundPriceByFormat (val, currency = 'ruble') {
  let type = null
  let value = 0
  let format = wNumb({ decimals: 0, thousand: ' ' })

  if (Number(val) > 1000) {
    type = 'thousand'
    value = val / 1000
  }

  if (Number(val) > 1000000) {
    type = 'million'
    value = val / 1000000
  }

  if (type !== null) {
    format = wNumb(FORMAT[type][currency])
    return format.to(value)
  }

  return format.to(Number(val))
}
