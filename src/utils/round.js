export function roundToMultiple (value, multiple) {
  return Math.round(value / multiple) * multiple
}
