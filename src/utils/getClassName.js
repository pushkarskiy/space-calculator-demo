export default (field, mod, style) => {
  let _mod = mod
    ? mod
      .split(' ')
      .map(item => {
        return style[item]
      })
      .join(' ')
    : ''
  return _mod.length ? `${style[field]} ${_mod} ` : style[field]
}
