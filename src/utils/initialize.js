import { render } from 'react-dom'
import autobind from 'autobind-decorator'

class Initializer {
  constructor(global) {
    this.widgets = {}

    global.addEventListener("DOMContentLoaded", this.init)
  }

  static widgetEntrySelector = '[data-widget-entry]'
  static widgetEntryAttr = 'data-widget-entry'
  static widgetTypeAttr = 'data-widget-type'
  static widgetConfigAttr = 'data-widget-config'

  @autobind
  init() {
    const entries = document.querySelectorAll(Initializer.widgetEntrySelector)

    for (let i = 0; i < entries.length; i++) {
      const appId = entries[i].getAttribute(Initializer.widgetEntryAttr)
      const type = entries[i].getAttribute(Initializer.widgetTypeAttr)
      const config = entries[i].getAttribute(Initializer.widgetConfigAttr)

      entries[i].removeAttribute(Initializer.widgetConfigAttr)

      this.widgets.hasOwnProperty(type)
        ? this.widgets[type](entries[i], { config: JSON.parse(config), appId })
        : console.warn(`Widget ${type} not found, entry id=${entries[i].getAttribute(Initializer.widgetEntryAttr)}`)
    }

  }

  @autobind
  registration(type, component) {
    if (!this.widgets.hasOwnProperty(type)) {
      this.widgets[type] = component
    }
  }
}

const initialize = (function () {
  if (!global.REACT_APPS || global.REACT_APPS && !global.REACT_APPS instanceof Initializer) {
    Object.assign(global, { REACT_APPS: new Initializer(global) })
  }

  return global.REACT_APPS
}(window !== undefined ? window : global))

export default initialize