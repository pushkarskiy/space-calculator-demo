import fetchJsonp from 'fetch-jsonp'

function error (error) {
  throw new Error(error.message)
}

function parseJSON (res) {
  return res.json()
}

export default url => {
  return fetchJsonp(url)
    .then(parseJSON)
    .catch(error)
}
