# Space calculator demo

### Зависимости

1. `nodejs` версии 8 или выше
2. `yarn` версии 0.18 или выше

### Запуск
- `yarn` - установить зависимости
- `yarn dev` - запуск в режиме разработке 
- `yarn dev:w` - запуск в режиме разработке на windows
- `yarn api` - запуск дев сервера 
- `yarn build:w` - сборка проекта в папку `/dist/dev` на windows
- `yarn prod` - сборка проекта в папку `/dist/prod`
- `yarn clean` -  чиста деректории `/dist`

### Структура проекта
```
├── /dist                          	
│       └── / prod                      # production distributive
│               ├── rev.json            # revmanifest for include file with hash
│               ....                    # scripts / css / images 
│       └── / dev                       # production distributive for debug mode 
│               ├── rev.json            # revmanifest for include file with hash
│               ....                    # scripts / css / images 
│
├── /conf                           	# configuration files
│       │
│       ├── app.js                      # app config default
│       ├── webpack.dev.js              # for development mode
│       ├── webpack.prod.js             # for production build
│       └── webpack.build.js            # for development build on live server, &feDebug=true
│
├── /server                         	# server
│       ├── index.js                	# simle server
│       └── routers.js              	# routers
│
└── /src
        ├── /components             	# components are responsible for how things work
        ├── /containers             	# containers are responsible for how things look
        ├── /utils                  	# utilities
        │       └── initialize.js      # script for initialize multi application on one page
        ├── /assets                 	# assets
        ├── /api                    	# the list of available APIs
        ├── /entries                    # widgets entry point
        └── /modules               	# widgets application 
```

### Как это работает ?
1. На страницу подлючаются бандлы виджетов и устанавливаются точки входа
```
<div data-widget-entry="widget[1]" data-widget-type="calc" data-widget-config=''></div>
```

 - `data-widget-entry` - Идентификатора приложения
 - `data-widget-type` - Тип виджета
 - `data-widget-config` - Конфиг для виджета в формате json
        
```
<!-- Не забываем про зависимости из CDN -->
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16.2.0/umd/react-dom-server.browser.production.min.js"></script>

<script type="application/javascript" src="/build/widget-common.js"></script>
<script type="application/javascript" src="/build/widget-calc.js"></script>
```
2. Каждый виджет регестрируется на странице по средством вызода метода `Initializer.registration('calc', init)`, где `calc` - название бандла, `init` - функция обьявленая в `entry` - точки входа, внутрь которой передатся объект `{ config, appId }`
3. После загрузки страницы на событие `DOMContentLoaded` вызовится метод `Initializer.init()`, который подключить в каждую точку входа нужный тип виджета и проинициализирует с переданным конфигом