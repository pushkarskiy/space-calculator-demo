import Koa from 'koa'
import logger from 'koa-logger'
import { api } from '../../conf/config.json'
import router from './router'
import bodyParser from 'koa-bodyparser'
import jsonp from 'koa-jsonp'

const app = new Koa()

// add host logging
app.use(async (ctx, next) => {
  console.log(`${ctx.protocol}://${ctx.host}`)
  await next()
})

// add logger
app.use(logger())

// body parser for POST request
app.use(bodyParser())

// jsonp
app.use(jsonp())

// set headers
app.use(async (ctx, next) => {
  ctx.set('Content-Type', 'application/json')
  ctx.set('Access-Control-Allow-Origin', '*')
  ctx.set(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  ctx.set('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
  ctx.set('Access-Control-Expose-Headers', 'Retry-After')

  await next()
})

// add routes
app.use(router.routes())

// process OPTIONS request
app.use(router.allowedMethods())

// listen port
app.listen(api.port, api.hostname)

console.log(`server running at http://${api.hostname}:${api.port}`)
