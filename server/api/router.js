import Router from 'koa-router'
import qs from 'query-string'

const router = new Router()

router.get('/get-objects', async ctx => {
  const data = {
    sales: rand(0, 100),
    rents: rand(0, 200),
  }

  ctx.body = {
    success: true,
    data: data,

    // For debug error
    error: { message: 'Objects not found' }
  }
})

function rand(min, max) {
  return Math.floor(Math.random() * (max - min) + min)
}

export default router
